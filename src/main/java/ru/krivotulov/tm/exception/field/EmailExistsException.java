package ru.krivotulov.tm.exception.field;

/**
 * LoginEmptyException
 *
 * @author Aleksey_Krivotulov
 */
public final class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error! user with this email not found...");
    }

}
