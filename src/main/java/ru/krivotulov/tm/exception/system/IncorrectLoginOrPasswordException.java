package ru.krivotulov.tm.exception.system;

import ru.krivotulov.tm.exception.AbstractException;

/**
 * IncorrectLoginOrPasswordException
 *
 * @author Aleksey_Krivotulov
 */
public final class IncorrectLoginOrPasswordException extends AbstractException {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}

