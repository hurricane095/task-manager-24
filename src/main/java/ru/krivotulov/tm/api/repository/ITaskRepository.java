package ru.krivotulov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task>{

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    Task create(@NotNull String userId,
                @NotNull String name,
                @NotNull String description);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
