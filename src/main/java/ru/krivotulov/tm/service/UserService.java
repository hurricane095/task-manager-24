package ru.krivotulov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.repository.IUserRepository;
import ru.krivotulov.tm.api.service.IUserService;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.exception.field.*;
import ru.krivotulov.tm.model.User;
import ru.krivotulov.tm.util.HashUtil;

import java.util.Optional;

/**
 * UserService
 *
 * @author Aleksey_Krivotulov
 */
public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public UserService(@NotNull final IUserRepository userRepository,
                       @NotNull final ITaskRepository taskRepository,
                       @NotNull final IProjectRepository projectRepository
    ) {
        super(userRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login,
                       @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        return repository.create(login, password);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login,
                       @Nullable final String password,
                       @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (isEmailExist(email)) throw new EmailExistsException();
        return repository.create(login, password, email);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login,
                       @Nullable final String password,
                       @NotNull final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        return repository.create(login, password, role);
    }

    @Nullable
    @Override
    public User delete(@Nullable final User model) {
        if(model == null) return null;
        @Nullable final User user = super.delete(model);
        if(user == null) return null;
        @NotNull final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Nullable
    @Override
    public User deleteByLogin(@Nullable final String login){
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return delete(user);
    }

    @Nullable
    @Override
    public User deleteByEmail(@Nullable final String email){
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return delete(user);
    }

    @Nullable
    @Override
    public User setPassword(@Nullable final String userId,
                            @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return Optional.ofNullable(findOneById(userId))
                .map(user -> {
                    @Nullable final String hash = HashUtil.salt(password);
                    user.setPasswordHash(hash);
                    return user;
                }).orElse(null);
    }

    @Nullable
    @Override
    public User updateUser(@Nullable final String userId,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return Optional.ofNullable(findOneById(userId))
                .map(user -> {
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    user.setMiddleName(middleName);
                    return user;
                }).orElse(null);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        Optional.ofNullable(user)
                .ifPresent(userModel -> userModel.setLocked(true));
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        Optional.ofNullable(user)
                .ifPresent(userModel -> userModel.setLocked(false));
    }

}
