package ru.krivotulov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.service.IAuthService;
import ru.krivotulov.tm.api.service.IUserService;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.exception.field.LoginEmptyException;
import ru.krivotulov.tm.exception.field.PasswordEmptyException;
import ru.krivotulov.tm.exception.system.AccessDeniedException;
import ru.krivotulov.tm.exception.system.IncorrectLoginOrPasswordException;
import ru.krivotulov.tm.exception.system.PermissionException;
import ru.krivotulov.tm.model.User;
import ru.krivotulov.tm.util.HashUtil;

import java.util.Arrays;

/**
 * AuthService
 *
 * @author Aleksey_Krivotulov
 */
public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(@Nullable final String login,
                      @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if(hash == null) throw new IncorrectLoginOrPasswordException();
        if (!hash.equals(user.getPasswordHash())) {
            throw new IncorrectLoginOrPasswordException();
        }
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Nullable
    @Override
    public User registry(@Nullable final String login,
                         @Nullable final String password,
                         @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public User getUser() {
        @NotNull final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        @Nullable final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
