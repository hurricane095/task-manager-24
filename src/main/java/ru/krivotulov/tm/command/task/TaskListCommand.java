package ru.krivotulov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

/**
 * TaskListCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Display task list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortType = TerminalUtil.readLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> taskList = getTaskService().findAll(userId, sort);
        renderTasks(taskList);
    }

}
