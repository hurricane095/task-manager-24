package ru.krivotulov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.List;

/**
 * TaskShowListByProjectCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskShowListByProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-list-by-project";

    @NotNull
    public static final String DESCRIPTION = "Display task list by project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.readLine();
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> taskList = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(taskList);
    }

}
