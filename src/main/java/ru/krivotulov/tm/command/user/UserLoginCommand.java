package ru.krivotulov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UserLoginCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-login";

    @NotNull
    public static final String DESCRIPTION = "Log in.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        @Nullable String login = TerminalUtil.readLine();
        System.out.println("ENTER PASSWORD: ");
        @Nullable String password = TerminalUtil.readLine();
        getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
