package ru.krivotulov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.service.IAuthService;
import ru.krivotulov.tm.api.service.IProjectService;
import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.util.DateUtil;

/**
 * AbstractProjectCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public String getUserId(){
        return getAuthService().getUserId();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("ID: " + project.getId());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
        @NotNull final Status status = project.getStatus();
        System.out.println("STATUS: " + status.getDisplayName());
    }

}
