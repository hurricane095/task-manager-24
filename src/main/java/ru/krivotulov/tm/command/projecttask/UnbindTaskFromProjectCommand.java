package ru.krivotulov.tm.command.projecttask;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UnbindTaskFromProjectCommand
 *
 * @author Aleksey_Krivotulov
 */
public class UnbindTaskFromProjectCommand extends AbstractProjectTaskCommand {

    @NotNull
    public static final String NAME = "unbind-task-from-project";

    @NotNull
    public static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("TASK ID: ");
        @Nullable final String taskId = TerminalUtil.readLine();
        System.out.println("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.readLine();
        @NotNull final String userId = getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, taskId, projectId);
    }

}
