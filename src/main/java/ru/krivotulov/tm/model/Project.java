package ru.krivotulov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.model.IWBS;
import ru.krivotulov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public Project(@NotNull final String name,
                   @NotNull final String description,
                   @NotNull final Status status,
                   @Nullable final Date dateBegin
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
