package ru.krivotulov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.krivotulov.tm.api.repository.ICommandRepository;
import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.repository.IUserRepository;
import ru.krivotulov.tm.api.service.*;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.command.project.*;
import ru.krivotulov.tm.command.projecttask.BindTaskToProjectCommand;
import ru.krivotulov.tm.command.projecttask.UnbindTaskFromProjectCommand;
import ru.krivotulov.tm.command.system.*;
import ru.krivotulov.tm.command.task.*;
import ru.krivotulov.tm.command.user.*;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.exception.system.ArgumentNotSupportedException;
import ru.krivotulov.tm.exception.system.CommandNotSupportedException;
import ru.krivotulov.tm.repository.CommandRepository;
import ru.krivotulov.tm.repository.ProjectRepository;
import ru.krivotulov.tm.repository.TaskRepository;
import ru.krivotulov.tm.repository.UserRepository;
import ru.krivotulov.tm.service.*;
import ru.krivotulov.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final static String PACKAGE_COMMANDS = "ru.krivotulov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService);


    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz : classes){
            registry(clazz);
        }
    }

    @SneakyThrows
    private void registry(@NotNull final Class clazz){
        if(Modifier.isAbstract(clazz.getModifiers())) return;
        if(!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final Object object = clazz.newInstance();
        final AbstractCommand command = (AbstractCommand) object;
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initUsers() {
        userService.create("user", "user", "usre@mail.ru");
        userService.create("admin", "admin", Role.ADMINISTRATOR);
    }

    private void init() {
        final String userId = userService.findByLogin("user").getId();
        projectService.create(userId, "proj", "proj");
        taskService.create(userId, "task", "task");
    }

    public void run(@Nullable final String[] args) {
        if (runArgument(args)) System.exit(0);
        initUsers();
        init();
        initLogger();
        process();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        subscribeSystemExit();
    }

    private void subscribeSystemExit() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void process() {
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.readLine();
                runCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void runCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private boolean runArgument(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        @Nullable final String param = args[0];
        runArgument(param);
        return true;
    }

    private void runArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

}
