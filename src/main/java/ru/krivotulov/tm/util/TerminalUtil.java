package ru.krivotulov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public interface TerminalUtil {

    @NotNull
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    @Nullable
    static Date nextDate() {
        try {
            @NotNull String value = bufferedReader.readLine();
            return DateUtil.toDate(value);
        } catch (@NotNull final IOException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    @Nullable
    static String readLine() {
        try {
            return bufferedReader.readLine();
        } catch (@NotNull final IOException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    @Nullable
    static Integer nextNumber() {
        try {
            @NotNull String value = bufferedReader.readLine();
            return Integer.parseInt(value);
        } catch (@NotNull final IOException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

}
